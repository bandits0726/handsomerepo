package com.thehandsome.app.dto;

public class ProductDTO {
	int id;
	int brandId;
	String sex;
	String season;
	String title;
	int price;
	String imagePath;
	int maincategoryId;
	int subcategoryId;
	String shortdetail;
	String longdetail;
}
